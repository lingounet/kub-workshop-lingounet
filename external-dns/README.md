# Gestion des URLs

Avant d'installer `external-dns`, il est nécessaire de créer un `ServiceAccount` afin de lui donner accès aux différents namespaces pour détecter les nouveaux endpoints à gérer:

```bash
kubectl apply -f external-dns/external-dns-rbac.yml
```

!!! success "ServiceAccount créé"
    ```console
    namespace/external-dns created
    serviceaccount/external-dns created
    clusterrole.rbac.authorization.k8s.io/external-dns created
    clusterrolebinding.rbac.authorization.k8s.io/external-dns-viewer created
    ```

## Configuration de external-dns pour CloudFlare

Vous aurez besoin de 2 infos : la clé d'API et le user email référencé par Cloudfare. Ces 2 infos sont stockés dans des variables d'environnement `API_KEY` & `API_MAIL`

!!! tip "Elles sont déjà configurées"
    On est sympa, c'est déjà fait grâce au script exécuté au début du workshop.
    Si vous avez changer de terminal, il faut refaire la commande suivante:
    ```bash
    --8<-- "initscript.sniplet"
    ```

### Gestion du secret pour accéder à l'API Cloudflare

Il est nécessaire de créer un secret pour stocker l'API key d'accès à Cloudflare et un autre pour le compte de connexion

```bash
kubectl -n external-dns create secret generic cloudflare-api-token --from-literal=api-key=$API_KEY
kubectl -n external-dns create secret generic cloudflare-user-mail --from-literal=user-mail=$API_MAIL
```

On peut vérifier que les secrets sont bien créés et disponibles


```bash
kubectl get secrets -n external-dns
```

!!! success "Secrets créés"
    ```console
    NAME                   TYPE     DATA   AGE
    cloudflare-api-token   Opaque   1      15s
    cloudflare-user-mail   Opaque   1      11s
    ```

## Installation d'external-dns

Pour déployer `external-dns`, il suffit de créer un `Deployment` installant l'image officielle d'`external-dns`

```bash
kubectl apply -f external-dns/external-dns-cloudflare.yml
```

Pour comprendre le manifest:

```yaml linenums="1" hl_lines="30 32 36 37 43 44 49 50"
--8<-- "external-dns/external-dns-cloudflare.yml"
```

??? info "Documentation officielle Cloudflare"
    La documentation officielle pour Cloudflare est [dispo](https://github.com/kubernetes-sigs/external-dns/blob/master/docs/tutorials/cloudflare.md)

!!! success "`external-dns` is Running"
    ```bash
    kubectl get po -n external-dns
    ```
    ```console
    NAME                                     READY   STATUS    RESTARTS   AGE
    external-dns-cloudflare-694f6f75-nf8n8   1/1     Running   0          2s
    ```

??? example "Vérifier les logs"
    On peut aussi vérifier que la connexion à Cloudflare est bien ok

    ```bash
    kubectl logs $(kubectl get po -n external-dns | grep external-dns-cloudflare | cut -d' ' -f1) -n external-dns
    ```

    ```console
    ...
    time="2023-12-20T08:52:41Z" level=info msg="Instantiating new Kubernetes client"
    time="2023-12-20T08:52:41Z" level=info msg="Using inCluster-config based on serviceaccount-token"
    time="2023-12-20T08:52:41Z" level=info msg="Created Kubernetes client https://10.3.0.1:443"
    ...
    ```

## External DNS en action

On peut voir dans les logs qu'`external-dns` a déjà automatiquement détecté notre `Ingress` précédent:

```bash
kubectl logs -f $(kubectl get po -n external-dns | grep external-dns-cloudflare | cut -d' ' -f1) -n external-dns
```

```console
...
time="2023-12-20T08:52:44Z" level=info msg="Changing record." action=CREATE record=new-deployment.mvt.grunty.uk ttl=1 type=A zone=be73d3e4c087b970da9bb670130a11fc
time="2023-12-20T08:52:45Z" level=info msg="Changing record." action=CREATE record=new-deployment.mvt.grunty.uk ttl=1 type=TXT zone=be73d3e4c087b970da9bb670130a11fc
time="2023-12-20T08:52:45Z" level=info msg="Changing record." action=CREATE record=a-new-deployment.mvt.grunty.uk ttl=1 type=TXT zone=be73d3e4c087b970da9bb670130a11fc
...
```

On peut vérifier que notre nom de domaine est bien reconnu

```bash
dig new-deployment.$TF_VAR_OVH_CLOUD_PROJECT_KUBE_NAME.grunty.uk @ara.ns.cloudflare.com
```

!!! success annotate "DNS est configuré avec notre IP publique"
    ```console
    ;; ANSWER SECTION:
    new-deployment.<votre_trigramme>.grunty.uk. 300 IN	A	57.128.120.31 (1)
    ```

1. 🛜 l'IP sera différente

!!! tip "On a changé l'image Docker entre temps"
    Ne vous étonnez pas si vous ne voyez plus le magnifique cinéma ASCII du départ.

    On a changé l'image Docker pour que ce que l'on affiche soit lisible lorsque l'on fait un cUrl

On peut aussi valider que le navigateur reconnait notre URL en visitant [http://new-deployment.{votre_trigramme}.grunty.uk/](http://new-deployment.<votre_trigramme>.grunty.uk)

ou via un cURL comme à l'étape précédente

```bash
curl new-deployment.<votre_trigramme>.grunty.uk
```

```console
Server address: 10.2.1.6:80
Server name: new-deployment-5998d8dbcc-kdbrk
Date: 20/Dec/2023:09:45:13 +0000
URI: /
Request ID: 82d85003846eed6c62744103d7ac2bda
```

C'est beaucoup plus pratique !! 🥳

Mais ... ce n'est pas très sécurisé le HTTP, le chef de brigade de la sécurité nous rappelle à l'ordre 🫣

**Si on ajoute quelques mL de sécurité avec des certificats pour notre HTTPs [➡️](../cert-manager/README.md)**
