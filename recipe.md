# Notre recette d'un cluster kub aux petits oignons

## La recette

Pour avoir un bon cluster:

- on l'instancie avec [terraform](terraform/README.md)
- on gère l'accès à nos endpoints depuis l'extérieur avec un **ingress-controller** tel que [nginx](nginx-ingress-controller/README.md)
- pour que ca soit lisible, on crée automatique des entrées DNS pour chaque nouveau endpoint avec [external-dns](external-dns/README.md)
- on sécurise tout ça avec des jolis certificats *Let's Encrypt* et à l'aide de [cert-manager](cert-manager/README.md)
- on externalise nos secrets dans GitLab avec [external-secrets](external-secrets/README.md)
- on contrôle un peu tout ce qu'il se passe dans la cocotte avec [kyverno](kyverno/README.md)
- une fois qu'on a bien travaillé, on autorise tout le monde à se reposer avec [kube-downscaler](kube-downscaler/README.md)

Miam 🤤

## Et ca sert à quoi ⁉️

Dans la vraie vie, ca nous sert à quoi.

Un premier exemple via l'utilisation des [environnements GitLab](https://docs.gitlab.com/ee/ci/environments/) pour simplement visualiser les différentes environnements d'un projet, quelle version/commit est déployé sur chacun, et faciliter leur listing et leur accès.

Aussi, on peut voir qu'avec une approche GitOps et [Flux](https://fluxcd.io/) par exemple, on peut facilement automatiser l'installation et la configuration de tout cela et de simplement mettre à jour tous nos clusters.

Essayons tout cela...

## C'est parti 🍝
Pour ces 2 exemples, il est nécessaire de **forker** le projet car vous aurez besoin d'être **Owner** du projet pour faire les manipulations.

Les 2 exercices sont indépendants, vous pouvez les faire dans l'ordre que vous souhaitez.

Pour celà, il suffit de cliquer sur [`Fork`](https://gitlab.com/yodamad-workshops/2024/devoxx/kub-workshop/-/forks/new) sur la page du projet et réaliser le fork de ce projet dans votre espace personnel.

![Configuration du fork](recipe/fork.png)

Il en reste plus qu'a cloner votre fork en local !

- Pour la recette des environnements Gitlab [➡️](gitlab/README.md).
- Pour la recette avec FluxCD [➡️](flux/README.md).

## Avant de partir 🧽 (**APRES** avoir fait les parties GitLab env. et/ou Flux)

Pensez à faire du ménage 🧹

1️⃣ Supprimer les ingress **avant** de supprimer le cluster (pour purger les records DNS):

  ```bash
  for i in $(kubectl get ingress -A --no-headers -o=name); do
    kubectl delete $i -n demos
  done
  ```

2️⃣ Supprimer le cluster:

```bash
cd terraform
terraform destroy 
```
