# Les GitLab environnements

## Pré-requis

Avant de démarrer cette partie, il est nécessaire de **forker** le projet car vous aurez besoin d'être **Owner** du projet pour faire les manipulations.
Pour cela, il suffit de [forker le projet](../recipe.md#cest-parti).

!!! info "Pas besoin de forker 2 fois"
    Si vous avez fait la partie [flux](../flux/README.md) avant celle-ci et que vous avez déjà forké le projet, pas besoin de faire un nouveau fork.
    Vous pouvez réutiliser le 1er.

!!! info "Gitpod"
    Si vous utilisez Gitpod n'oubliez pas de télécharger votre `kubeconfig` alias `cluster-ovh-${TF_VAR_OVH_CLOUD_PROJECT_KUBE_NAME}.yml`. Avec le fork vous allez démarrer avec un nouveau pod et par conséquence vos fichiers locaux ne seront plus accessibles.

## Installation

Pour installer l'agent, il faut suivre la [doc GitLab](https://docs.gitlab.com/ee/user/clusters/agent/) qui consiste à :

* ajouter le repo Helm pour GitLab

```bash
helm repo add gitlab https://charts.gitlab.io
helm repo update
```

* Et installer l'agent

!!! example annotate "Un exemple mais à récupérer depuis l'IHM GitLab à cause du token"
    ```bash
    helm upgrade --install demo-gitlab-env gitlab/gitlab-agent \
        --namespace gitlab-agent-demo-gitlab-env \
        --create-namespace \
        --set image.tag=v16.8.0-rc2 \ # (1)
        --set config.token=glagent-v... \ # (2)
        --set config.kasAddress=wss://kas.gitlab.com
    ```

1. 🔢 La version peut être différente
2. 🔑 Ce token est généré par GitLab au moment d'enregistrer l'agent.

!!! success "L'agent est installé correctement"
    ```console
    Thank you for installing gitlab-agent.

    Your release is named demo-gitlab-env.    
    ```

On vérifie que tout est ok

```bash
kubectl get po -n <namespace> # (1)
```
1. Namespace can be different

!!! success "Les pods gitlab-agent sont démarrés"
    ```console
    NAME                                              READY   STATUS    RESTARTS   AGE
    demo-gitlab-env-gitlab-agent-v1-8b8bc9c85-2tq2b   1/1     Running   0          2m48s
    demo-gitlab-env-gitlab-agent-v1-8b8bc9c85-w8wm7   1/1     Running   0          2m48s
    ```

## Utilisation

Grâce à l'agent on va pouvoir intéragir depuis GitLab-CI sans avoir besoin d'un `kubeconfig`

Par exemple, on peut déployer le helm custom disponible dans le répertoire `demo-gitlab`

On crée un job dans le fichier `.gitlab-ci.yml` à la racine du projet.

!!! info "Supprimer le job existant"
    Vous devez supprimer le contenu du `.gitlab-ci.yml` existant avant d'ajouter les éléments

```yaml
stages:
  - 🏗️ # (1)

🚧_deploy_env:
  stage: 🏗️
  image:
    name: dtzar/helm-kubectl
  script:
    - kubectl config use-context yodamad-workshops/2024/devoxx/kube-workshop:demo-gitlab-env # (2)
    - helm upgrade ${CI_COMMIT_REF_SLUG}-env ./demo-gitlab/ --set labName=${CI_PROJECT_NAME}-${CI_COMMIT_REF_SLUG} --install --create-namespace -n ${CI_PROJECT_NAME}-${CI_COMMIT_REF_SLUG}
  environment:
    name: ${CI_COMMIT_REF_SLUG}
    url : https://${CI_PROJECT_NAME}-${CI_COMMIT_REF_SLUG}.<votre_trigramme>.grunty.uk # (3)
  needs: []
```
1. ☢️ Ne pas oublier d'ajouter le stage du job dans la liste des stages
2. 🛰️ Ne pas oublier de changer le path
3. 🔢 Ne pas oublier de mettre votre trigramme

!!! info "Comprendre l'exemple"
    Dans cet exemple, on a plusieurs choses:

    - `kubectl config use-context` pour connecter la CI au cluster
    - `yodamad-workshops/2024/devoxx/kube-workshop` correspond au path vers votre projet forké sur gitlab.com. A adapter en fonction de vos données
    - `demo-gitlab-env` correspond au nom du projet. A adapter en fonction de vos données
    - `environment` : décrit l'environnement déployé en lien avec ce job
        * `url` : une URL dynamique qui est construit en fonction du nom du projet et de la branche
        * `on_stop` : l'action à déclencher quand l'environnement est arrêté, par exemple quand la branche associée est supprimée

!!! bug "Quel context utiliser"
    Si vous peinez à trouver quel est le bon nom du context à utiliser pour la partie `kubectl config use-context`, vous pouvez ajouter dans le job l'instruction suivante : `kubectl config get-contexts` qui vous listera les contextes disponibles.

    ```yaml
    🚧_deploy_env:
      [...]
        script:
          - kubectl config get-contexts
    ```

Grâce à ce que l'on a mis en place précédemment, on pourra simplement avoir un nouvel environnement avec une URL reconnnue (merci `external-dns`), sécurisée (merci `cert-manager`) pour que les devs et les testeurs du projet puissent accéder à la version souhaitée.

!!! tip "Mettre votre trigramme dans l'ingress"
    Dans le fichier `demo-gitlab/values.yaml`, il faut changer `<votre_trigramme>` par votre trigramme et commiter

On peut commiter le fichier `.gitlab-ci.yml`, et une fois que le pipeline est terminé (avec succès), on peut voir que tout est bien créé

```bash
kubectl get po -n gitlab-agent-demo-gitlab-env
```

!!! success "Tout est déployé"
    ```console
    NAME                                                  READY   STATUS    RESTARTS   AGE
    pod/demo-gitlab-env-gitlab-agent-v1-8b8bc9c85-2tq2b   1/1     Running   0          11h
    pod/demo-gitlab-env-gitlab-agent-v1-8b8bc9c85-w8wm7   1/1     Running   0          11h

    NAME                                              READY   UP-TO-DATE   AVAILABLE   AGE
    deployment.apps/demo-gitlab-env-gitlab-agent-v1   2/2     2            2           11h

    NAME                                                        DESIRED   CURRENT   READY   AGE
    replicaset.apps/demo-gitlab-env-gitlab-agent-v1-8b8bc9c85   2         2         2       11h
    ```

Pour continuer, créer une *nouvelle branche* et changer l'image par défaut dans `demo-gitlab/values.yml`:

Avant:

```yaml
  image: registry.gitlab.com/yodamad-workshops/kub-workshop/nginxdemos
  version: plain-text
```

Après:

```yaml
  image: registry.gitlab.com/yodamad-workshops/kub-workshop/asciinematic
  version: latest
```

Commiter sur la *nouvelle branche* et attendre que le pipeline se termine.

Dans le menu `Operate > Environments`, il y a 2 environnements disponibles:

![img.png](envs.png)

En ouvrant les 2 environnements (via `Open`), on a bien : 

- une IHM simple sur `main`
- un cinema ascii sur la nouvelle branche

## Nettoyage

Il ne faut pas oublier de nettoyer ses environements lorsqu'ils ne sont plus nécessaires (🌱 la planète vous dira merci).

Pour cela, on implémente le job qui est référencé dans le `on_stop`

```yaml
stages:
  [...]
  - 🏗️
  - 🧹 # (1)

🧼_clean:
  stage: 🧹
  image:
    name: dtzar/helm-kubectl
  script:
    - kubectl config use-context yodamad-workshops/kub-workshop:demo-gitlab-env
    - helm uninstall ${CI_COMMIT_REF_SLUG}-env -n ${CI_PROJECT_NAME}-${CI_COMMIT_REF_SLUG}
    - kubectl delete ns ${CI_PROJECT_NAME}-${CI_COMMIT_REF_SLUG}
  when: manual
  environment:
    name: ${CI_COMMIT_REF_SLUG}
    action: stop
```

Il faut également ajouter l'instruction `on_stop` au job de déploiement :

```yaml
🚧_deploy_env:
  [...]
  environment:
    [...]
    on_stop: 🧼_clean
```

1. ☢️ Ne pas oublier d'ajouter le stage du job dans la liste des stages

Arrêter l'environnement depuis l'IHM GitLab (via le menu `Operate > Environments`)

!!! success "L'environnement est bien supprimé"
    * Le job `on_stop` est bien déclenché

    ![clean](clean.png)

    * Dans `Operate > Environments`, il n'y a plus qu'un environnement
    * Dans le cluster, les élements ont bien été suppriméss

    ```bash
    kubectl get ns kub-workshop
    ```

    ```console
    Error from server (NotFound): namespaces "kub-workshop-snowcamp-2024-demo" not found
    ```

Un premier usage plutôt pratique !

**Retournons à la recette pour encore plus de découvertes [➡️](../recipe.md#et-ca-sert-à-quoi-⁉️)**
